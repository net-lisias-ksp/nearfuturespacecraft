# Near Future Spacecraft :: Change Log

* 2021-0826: 1.4.3 (Nertea) for KSP 1.12.0
	+ KSP 1.12
	+ Updated ModuleManager to 4.2.1
	+ Updated NearFutureProps to 0.6.5
	+ Added docking rotation functions to tiny shielded port
	+ Ajusted kerbal viewpoints for Nereid pod, should clip less
	+ Added more buoyancy to Nereid, should float ok with heatshield, chute and engines attached
* 2021-0721: 1.4.2 (Nertea) for KSP 1.12.0
	+ KSP 1.11
	+ Updated NearFutureProps to 0.6.5
	+ Updated B9PartSwitch to 2.18.0
	+ Fixed an issue with Waterfall light on Pandora pod
* 2020-1227: 1.4.1 (Nertea) for KSP 1.11.0
	+ Config hack to make cargo parts work in KSP 1.10.x
* 2020-1223: 1.4.0 (Nertea) for KSP 1.11.0
	+ KSP 1.11
	+ Updated NearFutureProps to 0.6.4
	+ Added inventories to all command parts
	+ Set up appropriate parts for cargo, etc
	+ Upgraded Pandora docking light to new light system
	+ Added Waterfall support to Pandora light
	+ Added Waterfall support to engines
* 2020-0803: 1.3.3 (Nertea) for KSP 1.10.1
	+ Fixed IVA portraits on Mk3-9 pod
* 2020-0803: 1.3.2 (Nertea) for KSP 1.10.1
	+ KSP 1.10.x
	+ Updated NearFutureProps to 0.6.3
	+ Updated B9PartSwitch to 2.17.0
	+ Updated ModuleManager to 4.1.4
	+ Adjusted a description to indicate that the Hummingbird does not get a podded variant (LouisB3)
	+ Various heatshield tweaks and tunings, including a patch for JNSQ reentries (Rodger)
	+ Improved Snacks! support (Michael Paul)
	+ Improved KerbalHealth support (LouisB3)
* 2020-0220: 1.3.1 (Nertea) for KSP 1.9.0
	+ KSP 1.9.x
	+ Updated NearFutureProps to 0.6.2
	+ Updated B9PartSwitch to 2.13.0
	+ Updated ModuleManager to 4.1.3
	+ Updates to Chinese localization (tinygrox)
* 2019-1106: 1.3.0 (Nertea) for KSP 1.8.1
	+ KSP 1.8.x
	+ Updated NearFutureProps to 0.6.1
	+ Updated B9PartSwitch to 2.12.1
	+ Updated ModuleManager to 4.1.0
	+ Updated ModuleManager pass specifiers where necessary
* 2019-0521: 1.2.4 (Nertea) for KSP 1.7.0
	+ Added patch for Universal Storage 2 adding nodes to the 3.75m service tank (thanks Zorg)
	+ Removed chicken from distribution
	+ Updated Russian localization (Sool3)
* 2019-0417: 1.2.3 (Nertea) for KSP 1.7.0
	+ KSP 1.7.x
	+ Updated NearFutureProps to 0.5.1
	+ Updated B9PartSwitch to 2.7.0
	+ Adjusted RCS part masses, costs and unlock costs to align with KSP 1.7 changes
* 2019-0320: 1.2.2 (Nertea) for KSP 1.6.1
	+ Hotfix broken texture file
* 2019-0319: 1.2.1 (Nertea) for KSP 1.6.1
	+ Hotfix version number
* 2019-0319: 1.2.0 (Nertea) for KSP 1.6.1
	+ Updated MM to 4.0.2
	+ Added French translation (don-vip)
	+ Soft-deprecated stack monopropellant tanks due to artistic irrelevance. Some may return later but for now you shouldn't build new craft with them
	+ Renamed PPD-24 to RPD-12 (name collision with SSPXr)
	+ Propagated command pod-related improvements from Restock to command pods (hatches, endcaps, colours)
	+ Retextured orbital engines completely
	+ Now uses improved thermal colorization method for engine emisives
	+ Redid orbital engine particle effects
	+ Orbital engine colour/manufacturer are now consistent with Reaction Engines (stock Puff)
	+ Minor texture tunings in many places
	+ Fixed floating Occupy Duna poster in Elara IVA
	+ Fixed top node of Callisto being a bit too high
	+ Added support for the Landertron mod to the three landing command pods (DStaal)
	+ Added support for newer versions of USI-LS (dlrk2)
	+ Recompressed many textures with higher quality compressor
* 2019-0121: 1.0.3 (Nertea) for KSP 1.6.1
	+ Added German translation from LeLeon and Three_Pounds
* 2019-0117: 1.0.2 (Nertea) for KSP 1.6.1
	+ KSP 1.6.x
	+ Updated B9PartSwitch to 2.6.0
	+ Updated ModuleManager to 3.1.3
	+ Updated NFProps to 0.5.0
	+ Fixed tiny docking nose not having a docking node
	+ Changed licensing of code/configs to MIT
* 2018-1116: 1.0.1 (Nertea) for KSP 1.5.1
	+ Decreased Elara body lift from 0.5 to 0.2
	+ Added action group spec to Elara hatch, added deploy limit
	+ Added toggleable docking spotlight to Pandora chin area
	+ Added a collider to the Docking monitor in the Pandora interior, if you click on it, it will change to a external view.
	+ Added consistent alternate RCS layout names to relevant pods for modder use
	+ Fixed a stray RCS thruster on the Callisto
	+ Moved Callisto flag so it doesn't obscure the surface camera.
	+ Tweaked UV of Nereid flag
	+ Fixed typo in Hummingbird engine description
	+ Moved Nereid pod colliders up a bit, fixes accessibility of engines
	+ Fixed localization of Nereid alternate engines pod versions
* 2018-1112: 1.0.0 (Nertea) for KSP 1.4.4
	+ KSP 1.5.1
	+ Updated B9PartSwitch to 2.4.5
	+ Updated ModuleManager to 3.1.0
	+ Updated NF Props to 0.3.5
	+ Removed MiniAVC distribution
	+ Added 0.625m docking nosecone
	+ Added 0.625m blunt shaped nosecone
	+ Added Mk1-X 'Phoebe' Orbital Command Pod: orbitally focused 1 seater 1.25m command pod
	+ Added Mk1-TMA 'Proteus' Advanced Command Pod: advanced 2 seater 1.25m command pod
	+ Added Mk1-L 'Nereid' Command Pod: specialized 1.25m command pod for landing 2 kerbals (Dragon 2 style)
	+ Added Mk3B 'Pandora' Advanced Command Pod: advanced 4 seater 2.5m command pod
	+ Added Mk3 'Tethys' Advanced Command Pod: specialized 2.5m command pod for landing 3 kerbals (Dragon 2 style)
	+ Added Mk4-B 'Elara' Biconic Command Module: high tech multipurpose 3.75m command pod
	+ Added 64-8S 'Chickadee' Landing Engine: high thrust small radial monoprop engine for Nereid, Tethys
	+ Added 96-8S 'Mockingbird' Landing Engine: higher thrust larger radial monoprop engine for Almathea
	+ Added new variant to Mk4-1 pod: shroud option to enable landing pod nodes in lower skirt with extra integrated fuel
	+ Added heat shield module to Mk4-1 pod: Ablator is added when using the either of the lower skirt modes that terminate at 3.75m
	+ Rebalanced Mk4-1 pod to fit the "landing pod" paradigm for 3.75m
	+ Added nicknames to all old command pods (Mk4-1 Almathea, PPD-1 Callisto, Mk 3-9 Rhea)
	+ Added integrated RCS to Mk3-9, PPD-1 and Mk4-1 pods
	+ Reduced mass of Mk3-9 pod to 1.85 from 2.1
	+ Redid external textures of Mk3-9, PPD-1 and Mk4-1 to match new standards (consistent colours, etc)
	+ Retextured Itinerant pod to match standards and fixed its orientation
	+ New RCS FX for improved RCS parts
	+ Rearranged CTT nodes somewhat
* 2018-0807: 0.7.10 (Nertea) for KSP 1.4.2
	+ KSP 1.4.5
	+ Updated B9Partswitch to 2.3.3
	+ Updated ModuleManager to 3.0.7
* 2018-0503: 0.7.9 (Nertea) for KSP 1.4.2
	+ KSP 1.4.3
	+ Updated B9Partswitch to 2.3.0
	+ Updated NF Props to 0.3.3
	+ Switched versioning to mix/max specification
	+ Disabled drag cube recalculation for B9PS-using engines to work around stock issue
* 2018-0410: 0.7.8 (Nertea) for KSP 1.3.1
	+ KSP 1.4.2
	+ Updated B9PartSwitch to 2.2.1
	+ Updated NF Props to 0.3.2
	+ Fixed KerbalHealth patch
* 2018-0303: 0.7.7 (Nertea) for KSP 1.3.1
	+ Fixed TAC patch problem.
* 2018-0302: 0.7.6 (Nertea) for KSP 1.3.1
	+ Updated ModuleManager to 3.0.4
		- Updated B9PartSwitch to 2.1.1
		- Updated NFProps to 0.3.0
		- Added KerbalHealth patch (Fraz86)
		- Added fix for TAC-LS (Streetwind)
* 2017-1215: 0.7.5 (Nertea) for KSP 1.3.1
	+ Updated B9PartSwitch to 2.1.0
	+ Updated NFProps to 0.2.1
	+ Added Simplified Chinese translation
	+ Updates to Russian translation
	+ Some IVA fixes
	+ Increased skin max temp of LT-POD legs to 2800K, increased emissive constant to 0.9
	+ Added RPM support back to the mod
		- Courtesy of Dragon01
		- Requires the installation of ASET Props and Raster Prop Monitor
* 2017-1027: 0.7.4 (Nertea) for KSP 1.3.0
	+ KSP 1.3.1
	+ Dependency update
	+ Updated NFProps to 0.2.0
		- Better in every way
* 2017-0804: 0.7.3 (Nertea) for KSP 1.3.0
	+ Updated MM to 2.8.1
	+ Reexported normal maps
	+ Reduced lookup radius of 3.75m service tank's occlusion checker (was 2x too high)
	+ Updated B9PartSwitch to 1.9.0
	+ Added Russian translation courtesy of Dr. Jet
* 2017-0626: 0.7.2 (Nertea) for KSP 1.3.0
	+ Updated B9PartSwitch to 1.8.1
	+ Reenabled drag cube switches on Mk4-1 pod due to B9PartSwitch update
	+ Fixed renamed parts not being handled by CTT/USI/CLS
	+ Spanish Translation courtesy of forum user fitiales
* 2017-0620: 0.7.1 (Nertea) for KSP 1.3.0
	+ Disabled drag cube switches on Mk4-1 pod, fixes map view issues but drag might be a little weird until this is really fixed
* 2017-0616: 0.7.0 (Nertea) for KSP 1.2.2
	+ OLD ORBITAL ENGINES FULLY DEPRECATED
	+ KSP 1.3
	+ Updated bundled MM to 2.8.0
	+ Updated bundled B9PartSwitch to 1.8.0
	+ Full localization support for all parts
	+ New textures for Mk4-1, Mk3-9 and PPD-1 pods
	+ New model and texture for PPD-24 Itinerant
	+ Improved textures for all monopropellant tanks and service tanks
	+ Mk4-1 pod now has toggleable shroud "skirts" that change attachment options and slightly increase thermal tolerances
	+ Checked the exhaust transforms on all RCS parts
	+ Reduced vacuum Isp of all monopropellant engines by 10-20s
	+ All RCS parts have been set to physicsless
	+ Many IVA-related fixes
	+ Fixed USI-LS patch for Mk4-1 pod
	+ Improved Mk 3-9, PPD-24 and PPD-1 IVAs
		- Fixed some holes
		- Better lighting
		- Better diffuse map, new normal maps
		- Realigned and improved props
		- Improve Mk 4-1 IVA
	+ Fixed some holes
		- better lighting
		- Realigned props
		- IVA RPM integration disabled for now
		- NF Props updated
	+ Now versioned (0.1.0 is current)
		- Removed many less useful props
	+ Fixed transform centering of remaining props
	+ RPM functions of RPM-enabled props are now applied using MM, avoiding error messages when RPM is not installed
* 2017-0309: 0.6.3 (Nertea) for KSP 1.2.2
	+ Fixed NFProps not being included… again
	+ Fixed resource flow modes for new monopropellant engines so that they mimic LFO fuel modes
	+ Fixed orientation of Mk4-1 IVA windows
* 2017-0208: 0.6.2 (Nertea) for KSP 1.2.2
	+ Updated B9PartSwitch to 1.7.1
	+ Tweaked orbital engine FX
	+ Fixed thrust angle deviation on LV-601-4 and LV-85-6 engines
	+ Fixed overly large engine temperature tolerances
	+ Fixed botched normal map names for a few parts
	+ Extras/LFO patch now converts orbital engine integrated tanks to LF/O as well
* 2017-0205: 0.6.1 (Nertea) for KSP 1.2.2
	+ Fixed incorrect part names on some engines
	+ Fixed swapped FL-R-B3000 and FL-R-B750 costs
	+ Fixed accidentally included skirt node of mk4 pod
	+ Updated deprecated part loading
	+ Added an optional Extras patch to make the orbital engines use LFO!
* 2017-0204: 0.6.0 (Nertea) for KSP 1.2.1
	+ Marked for KSP 1.2.2
	+ Fixed missing NearFutureProps distribution
	+ Added Mk4-1 Command Pod, 7-kerbal large command pod
	+ Added LV-85 Orbital Maneuvering Engine: 0.625m monopropellant engine that replaces LV-T95 engine
	+ Added LV-601 Orbital Maneuvering Engine: 1.25m monopropellant engine
	+ Added LV-85-6 Orbital Maneuvering Engine Cluster: 2.5m monopropellant engine that replaces LV-T95x8 engine
	+ Added LV-601-4 Orbital Maneuvering Engine Cluster: 3.75m monopropellant engine that replaces LV-T18 engine
	+ Added FL-R-A750, FL-R-A375, FL-R-A185 1.25m Monopropellant tanks
	+ Added FL-R-B750, FL-R-B1500, FL-R-B3000 2.5m Monopropellant tanks
	+ Added FL-R-C1500, FL-R-C3000, FL-R-C6000 3.75m Monopropellant tanks
	+ Added SD-01 and SD-02 radial engine pods
	+ Improved cargo bay shielding boxes for 3.75m service tank
	+ Soft-deprecated LV-T95, LV-T95x8, LVT18 engines (will not break ships, no longer visible in VAB/SPH)
* 2016-1118: 0.5.4 (Nertea) for KSP 1.2
	+ Marked for KSP 1.2.1
	+ USI-LS compatibility patch update
	+ Added 'nearfuture' search tag to parts that were missing it
	+ Fixed some description typos
	+ Tweaked cost of RCS thrusters
	+ Added RX-55 RCS block (5 nozzles)
	+ Added RX-45 RCS block (4 nozzles)
	+ Added RX-15 RCS block (3 nozzles)
* 2016-1027: 0.5.3 (Nertea) for KSP 1.2
	+ Parts with window lights are now properly tied to the global Light action group
	+ Changed LT-POD landing leg to Ground category
	+ Added audio cues to LT-POD landing leg
	+ Fixed orientation of LT-POD landing foot
	+ Improved general performance of LT-POD landing foot, still not perfect but progressing
	+ Mk 3-9 pod flag transform is no longer visible on the part icon
	+ Fixed strange colliders on 2.5m service tank
	+ Disabled surface attachment on doors of 3.75m service tank
* 2016-1021: 0.5.2 (Nertea) for KSP 1.1.3
	+ KSP 1.2
	+ Changed service tanks to Payload category
	+ Converted RCS blocks to ModuleRCSFX, adds 1.2 sounds and FX
* 2016-0626: 0.5.1 (Nertea) for KSP 1.1.2
	+ KSP 1.1.3
	+ Reexported collider of PPD-1 command pod
* 2016-0518: 0.5.0 (Nertea) for KSP 0.7.3
	+ No changelog provided
